# Build a Simple Transaction with `cardano-cli`
What's really happening when you send ADA from your Cardano wallet? 

Let's take a look.

## Pre-Requisites
In order to follow along with this tutorial, you'll need a to be running a local instance of `cardano-node`. You can see the documentation here.

It's not trivial to run `cardano-node`, so don't worry if you don't have that.

At Gimbalabs, we are working on ways to make it easier for you to interact with the Cardano protocol. The purpose of this tutorial is to provide with background knowledge so that you have a better idea of how the architecture works. We hope that this will help you get better at imagining solution architecture.

## Step 0: Set some variables
Just to make the rest of this tutorial a little bit easier, let's set some variables. We're using some basic functionality of bash scripting. Don't worry if you've never done this before - our goal is just to take a look at what is going on "under the hood" when you're sending ADA. Of course, if you'd like to learn a little bit of bash, [here's an excellent place to start](https://www.shellscript.sh/)

## Step 0: Set some variables
```
SENDER="address"
RECEIVER="address"
SENDERKEY="...payment.skey" (this is a file)
MYTXIN="..."
```
## Step 1: Get protocol parameters
```
cardano-cli query protocol-parameters \
--mainnet \	
--out-file protocol.json
```
Check out that file!

## Step 2: Get info about the UTXO
`cardano-cli query utxo --address $SENDER --mainnet`

## Step 3: Draft the transaction
```
cardano-cli transaction build-raw \
--tx-in $MYTXIN \
--tx-out $RECEIVER1+0 \
--tx-out $RECEIVER2+0 \
--tx-out $SENDER+0 \
--invalid-hereafter 0 \
--fee 0 \
--out-file tx.draft
```

We can now take a look at this transaction draft. If you open the file, you'll see a hash, but this doesn't tell you much.

Instead, try: 
```
cardano-cli transaction view --tx-body-file tx.draft
```

Take a look at the results.


## Step 4: Calculate Fees
```
cardano-cli transaction calculate-min-fee \
--tx-body-file tx.draft \
--tx-in-count 1 \
--tx-out-count 3 \
--witness-count 1 \
--byron-witness-count 0 \
--mainnet \
--protocol-params-file protocol.json
```

We can now see a certain number of `Lovelace` - does this number look familiar?

## Step 5: Calculate the change
We can set some variables here too.

```
FEE=176413
SEND=2000000
CHANGE=1823587
```

## Step 6: Check the slotNo
`cardano-cli query tip --mainnet`

We can set an expiration:
```
EXPIRE=...
```

## Step 7: Build the tx
```
cardano-cli transaction build-raw \
--tx-in $MYTXIN \
--tx-out $RECEIVER+$SEND \
--tx-out $SENDER+$CHANGE \
--invalid-hereafter $EXPIRE \
--fee $FEE \
--out-file tx.raw
```

## Step 8: Sign the tx
```
cardano-cli transaction sign \
--tx-body-file tx.raw \
--signing-key-file $SENDERKEY \
--mainnet \
--out-file tx.signed
```

## Step 9: Submit it!
```
cardano-cli transaction submit \
--tx-file tx.signed \
--mainnet
```

